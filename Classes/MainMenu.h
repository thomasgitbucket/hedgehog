//
//  MainMenu.h
//  SwipyHedgehog
//
//  Created by Leander on 04/12/14.
//
//

#ifndef __SwipyHedgehog__MainMenu__
#define __SwipyHedgehog__MainMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "Box2D.h"
#include "GLES-Render.h"
#include "Hero.h"
#include "MudPiece.h"
#include "CCGestureRecognizer/CCSwipeGestureRecognizer.h"
#include "SocailBridge.h"
using namespace cocos2d;
class MainMenu : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    CCLayer* main;
    void enter_game(CCObject* sender);
    void replay_game(CCObject* sender);
    void share_game(CCObject* sender);
    void highscore_menu(CCObject* sender);
    void removeCloud(CCObject* sender);
    void remove_label(CCObject* sender);
    void remove_apple(float dt);
    void update(float dt);
    void tick(float dt);
    void update_leaf(float dt);
    void waiting_play(float dt);
    void start_game(float dt);
    void create_mud(float dt);
    void moving_left(float dt);
    void moving_right(float dt);
    void remove_pastObject(float dt);
    void collision_detect();
    
    CCArray* muds;
    CCArray* inners;
    int timer,timer_leaf,hero_speed,score,real_score,highscore;
    CCLabelTTF* label_score;
    CCLabelTTF* score_label_value;
    CCLabelTTF* best_label_value;
    CCLabelTTF* score_label_value_main;
    CCLabelTTF* best_label_value_main;
    CCMenu *menu_overlay;
    CCSprite* pSprite;
    CCSprite* sprite;
    CCSprite* overlay;
    CCSprite* overlay_score;
    CCSprite* overlay_gameover;
    CCSprite* apple;
    CCSprite* tutorial_image;
    CCSprite* inner_mud;
    Hero* character_hero;
    CCSprite* main_hero;
    void initPhysics();
    void draw();
    bool flag, paused, is_scoreboard;
    bool score_pos_flag,swipe_flag;
    
    GameSetting* settings;
    
    b2ContactListener* collisionlistner;
    b2Body *body_apple;
    b2Body *mud1;
    b2Body *mud2;
    b2Body *body_hero;
    float hero_hight;
    float temp_pos,inner_temp_pos;
    // implement the "static node()" method manually
    CREATE_FUNC(MainMenu);
    
    
private:    
    void swipe_L2R(CCObject* sender);
    void swipe_R2L(CCObject* sender);
    void swipe_B2T(CCObject* sender);
    void swipe_T2B(CCObject* sender);
    void init_swipeGesture();
    void movetoCenter(float dt);
    
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    
    GLESDebugDraw *m_debugDraw;
    b2World* world;
    enum swipe_direction{
        s_L2R = 50,
        s_R2L,
        s_T2B,
        s_B2T,
    };
};


#endif /* defined(__SwipyHedgehog__MainMenu__) */

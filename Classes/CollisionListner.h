//
//  CollisionListner.h
//  SwipyHedgehog
//
//  Created by lion on 1/25/15.
//
//

#ifndef __SwipyHedgehog__CollisionListner__
#define __SwipyHedgehog__CollisionListner__

#include "Box2D.h"
#include "cocos2d.h"
#include "GameSetting.h"

using namespace cocos2d;

class CollisionListner : public b2ContactListener
{
public:
    CollisionListner();
    ~CollisionListner();
    GameSetting* settings;
    virtual void BeginContact(b2Contact* contact);
    virtual void EndContact(b2Contact* contact);
    virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
    virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
};

#endif /* defined(__SwipyHedgehog__CollisionListner__) */

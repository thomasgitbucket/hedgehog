//
//  MainMenu.cpp
//  SwipyHedgehog
//
//  Created by Leander on 04/12/14.
//
//

#include "MainMenu.h"
#include "GameSetting.h"
#include "cocos2d.h"
#include "GB2ShapeCache-x.h"
#include "CollisionListner.h"
#include "SplashScene.h"
USING_NS_CC;
using namespace cocos2d;
CCScene* MainMenu::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // 'layer' is an autorelease object
    MainMenu *layer = MainMenu::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    main = new CCLayer();
    this->addChild(main,1);
    
    settings = GameSetting::sharedGameSetting();
    hero_speed = -12;
    score = 0;
    real_score = 0;
    score_pos_flag = false;
    swipe_flag = false;
    paused = false;
    is_scoreboard = false;
    init_swipeGesture();
    muds = CCArray::create();
    muds->retain();
    inners = CCArray::create();
    inners->retain();
    this->setTouchEnabled(true);
    this->setAccelerometerEnabled(true);
    character_hero = new Hero();
    timer=0;
    temp_pos = 88.5f;
    inner_temp_pos = 0.0f;
    timer_leaf = 0;
    //    float width =   CCDirector::sharedDirector()->getWinSize().width;
    //    float height = CCDirector::sharedDirector()->getWinSize().height;
    //    CCLog("w= %f,h= %f" ,CCDirector::sharedDirector()->getVisibleSize().width,CCDirector::sharedDirector()->getVisibleSize().height);
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
    if (IS_IPAD)
    {
        pSprite  = CCSprite::create("bg_mainmenu.png");
        pSprite->setScaleX(DEVICE_WIDTH/pSprite->getContentSize().width);
        pSprite->setScaleY(SCREEN_HEIGHT/pSprite->getContentSize().height);
        pSprite->setPosition(ccp(SCREEN_WIDTH/2,SCREEN_HEIGHT-pSprite->getContentSize().height/2*(SCREEN_HEIGHT/1024)));
    }
    else
    {
        pSprite  = CCSprite::create("bg_mainmenu_iphone.png");
        pSprite->setScaleX(DEVICE_WIDTH/pSprite->getContentSize().width);
        pSprite->setScaleY(DEVICE_HEIGHT/pSprite->getContentSize().height);
        pSprite->setPosition(ccp(SCREEN_WIDTH/2,SCREEN_HEIGHT-pSprite->getContentSize().height/2*(SCREEN_HEIGHT/1136)));
    }
    
    main->addChild(pSprite);
    
    CCSprite* tree = CCSprite::create("tree.png");
    
    if(IS_IPAD)
        tree->setPosition(ccp(origin.x+tree->getContentSize().width/2,pSprite->getContentSize().height*0.545f));
    else
        tree->setPosition(ccp(origin.x+tree->getContentSize().width*0.45f,pSprite->getContentSize().height*0.52f));
    
    
    main->addChild(tree,5);
    
    
    CCSprite* house = CCSprite::create("house.png");
    if (IS_IPAD)
        house->setPosition(ccp(SCREEN_WIDTH-house->getContentSize().width/2, pSprite->getContentSize().height*0.58f));
    else
        house->setPosition(ccp(640.0f-house->getContentSize().width/2, pSprite->getContentSize().height*0.58f));
    pSprite->addChild(house);
    
    
    overlay = CCSprite::create("overlay.png");
    overlay->setPosition(ccp(SCREEN_WIDTH/2, SCREEN_HEIGHT*0.7f));
    overlay->setScale(SCALE_SCREEN_ORIGIN);
    main->addChild(overlay,5);
    
    overlay_score = CCSprite::create("overlay.png");
    overlay_score->setPosition(ccp(SCREEN_WIDTH/2, SCREEN_HEIGHT*0.7f));
    overlay_score->setScale(SCALE_SCREEN_ORIGIN);
    main->addChild(overlay_score,6);
    overlay_score->setVisible(false);
    
    
    CCSprite* score_label_main = CCSprite::create("score_label.png");
    score_label_main->setPosition(ccp(overlay->getContentSize().width*0.38f, overlay->getContentSize().height*0.65f));
    //    score_label_main->setScale(1.5f);
    overlay_score->addChild(score_label_main);
    score_label_value_main = CCLabelTTF::create("0", "", 84);
    //    score_label_value_main->setScale(1.5f);
    score_label_value_main->setColor(ccBLACK);
    score_label_value_main->setPosition(ccp(overlay->getContentSize().width*0.75f, overlay->getContentSize().height*0.65f));
    overlay_score->addChild(score_label_value_main);
    
    CCSprite* best_label_main = CCSprite::create("best_label.png");
    best_label_main->setPosition(ccp(overlay->getContentSize().width*0.38f, overlay->getContentSize().height*0.35f));
    //    best_label_main->setScale(1.5f);
    overlay_score->addChild(best_label_main);
    best_label_value_main = CCLabelTTF::create("0", "", 84);
    //    best_label_value_main->setScale(1.5f);
    best_label_value_main->setColor(ccBLACK);
    best_label_value_main->setPosition(ccp(overlay->getContentSize().width*0.75f, overlay->getContentSize().height*0.35f));
    overlay_score->addChild(best_label_value_main);
    
    
    
    
    overlay_gameover = CCSprite::create("overlay.png");
    overlay_gameover->setScale(SCALE_SCREEN_ORIGIN);
    overlay_gameover->setVisible(false);
    
    
    CCMenuItemImage *btn_replay = CCMenuItemImage::create("btn_replay.png", "btn_replay.png", this, menu_selector(MainMenu::replay_game));
    btn_replay->setPosition(ccp(overlay->getContentSize().width*0.3f, overlay->getContentSize().height*0.65f));
    CCMenuItemImage *btn_share = CCMenuItemImage::create("btn_share.png", "btn_share.png", this, menu_selector(MainMenu::share_game));
    btn_share->setPosition(ccp(overlay->getContentSize().width*0.7f, overlay->getContentSize().height*0.65f));
    CCMenu *menu_over = CCMenu::create(btn_replay,btn_share,NULL);
    menu_over->setPosition(CCPointZero);
    overlay_gameover->addChild(menu_over);
    overlay_gameover->setPosition(ccp(SCREEN_WIDTH/2, SCREEN_HEIGHT*0.7f));
    
    CCSprite* score_label = CCSprite::create("score_label.png");
    score_label->setPosition(ccp(overlay->getContentSize().width*0.25f, overlay->getContentSize().height*0.3f));
    overlay_gameover->addChild(score_label);
    score_label->setScale(0.5f);
    score_label_value = CCLabelTTF::create("0", "", 48);
    score_label_value->setPosition(ccp(overlay->getContentSize().width*0.45f, overlay->getContentSize().height*0.3f));
    overlay_gameover->addChild(score_label_value);
    
    CCSprite* best_label = CCSprite::create("best_label.png");
    best_label->setPosition(ccp(overlay->getContentSize().width*0.65f, overlay->getContentSize().height*0.3f));
    best_label->setScale(0.5f);
    overlay_gameover->addChild(best_label);
    best_label_value = CCLabelTTF::create("0", "", 48);
    best_label_value->setPosition(ccp(overlay->getContentSize().width*0.8f, overlay->getContentSize().height*0.3f));
    overlay_gameover->addChild(best_label_value);
    
    this->addChild(overlay_gameover,5);
    
    
    tutorial_image = CCSprite::create("tutorial.png");
    tutorial_image->setScale(SCALE_SCREEN_ORIGIN);
    tutorial_image->setVisible(false);
    this->addChild(tutorial_image,5);
    
    main_hero = character_hero->getHero();
    main_hero->setPosition(ccp(SCREEN_WIDTH*0.47f, SCREEN_HEIGHT*0.38f));
    main->addChild(main_hero,4);
    
    CCMenuItemImage *btn_play = CCMenuItemImage::create("btn_play.png", "btn_play.png", this, menu_selector(MainMenu::enter_game));
    btn_play->setPosition(ccp(overlay->getContentSize().width*0.3f, overlay->getContentSize().height*0.5f));
    CCMenuItemImage *btn_score = CCMenuItemImage::create("btn_highscore.png", "btn_highscore.png", this, menu_selector(MainMenu::highscore_menu));
    btn_score->setPosition(ccp(overlay->getContentSize().width*0.7f, overlay->getContentSize().height*0.5f));
    menu_overlay = CCMenu::create(btn_play,btn_score,NULL);
    menu_overlay->setPosition(CCPointZero);
    overlay->addChild(menu_overlay);
    
    update(0.0f);
    schedule(schedule_selector(MainMenu::update), 8.0f);
    schedule(schedule_selector(MainMenu::update_leaf));
    
    initPhysics();
    CCPoint apple_pos;
    if (IS_IPAD)
        apple_pos =  CCPointMake(SCREEN_WIDTH*0.22f, SCREEN_HEIGHT*0.63f);
    else
        apple_pos =  CCPointMake(SCREEN_WIDTH*0.3f, SCREEN_HEIGHT*0.63f);
    
    body_apple = MudPiece::create_apple(main, world, apple_pos);
    mud1 = MudPiece::create_first_muds(main, world, ccp(0.0f,0.0f), type_mud1);
    mud2 = MudPiece::create_first_muds(main, world, ccp(0.0f, 0.0f), type_mud2);
    
    muds->addObject((CCObject*)mud1);
    muds->addObject((CCObject*)mud2);
    
    inner_mud = CCSprite::create("inner_mud.png");
    inner_mud->setPosition(ccp(apple_pos.x,inner_mud->getContentSize().height/2));
    CCLog("=====================%f",inner_mud->getContentSize().height/2);
    main->addChild(inner_mud,-1);
    inners->addObject(inner_mud);
    

    
    label_score = CCLabelTTF::create("", "", 36);
    label_score->setPosition(ccp(500.0f, 500.0f));
    this->addChild(label_score,100);
    label_score->setVisible(false);
    
    return true;
}
void MainMenu::initPhysics()
{
    GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile("muds.plist");
    b2Vec2 gravity;
    gravity.Set(0.0f, -8.0f);
    world =new b2World(gravity);
    world->SetAllowSleeping(true);
    world->SetContinuousPhysics(true);
    collisionlistner = new CollisionListner();
    world->SetContactListener(collisionlistner);
    m_debugDraw = new GLESDebugDraw(PTM_RATIO);
    world->SetDebugDraw(m_debugDraw);
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    flags += b2Draw::e_jointBit;
    m_debugDraw->SetFlags(flags);
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(SCREEN_WIDTH/2/PTM_RATIO, SCREEN_HEIGHT/2/PTM_RATIO);
    b2Body* groundBody = world->CreateBody(&groundBodyDef);
    b2PolygonShape groundBox;
    
    //bottom
    //    groundBox.SetAsBox(SCREEN_WIDTH/2/PTM_RATIO, 0, b2Vec2(0, -SCREEN_HEIGHT/2/PTM_RATIO), 0);
    //    groundBody->CreateFixture(&groundBox, 0);
    
    // top
    groundBox.SetAsBox(SCREEN_WIDTH/2/PTM_RATIO, 0, b2Vec2(0, SCREEN_HEIGHT/2/PTM_RATIO), 0);
    groundBody->CreateFixture(&groundBox, 0);
    
    // left
    groundBox.SetAsBox(0, SCREEN_HEIGHT/2/PTM_RATIO, b2Vec2(-SCREEN_WIDTH/2/PTM_RATIO, 0), 0);
    groundBody->CreateFixture(&groundBox, 0);
    
    // right
    groundBox.SetAsBox(0, SCREEN_HEIGHT/2/PTM_RATIO, b2Vec2(SCREEN_WIDTH/2/PTM_RATIO, 0), 0);
    groundBody->CreateFixture(&groundBox, 0);
    
    
    
    flag = true;
    
}
void MainMenu::update(float dt)
{
    
    CCSprite* cloud = CCSprite::create("cloud.png");
    pSprite->addChild(cloud,5);
    float k = (arc4random()%10)/100.0f+0.85f;
    cloud->setPosition(ccp(pSprite->getContentSize().width+cloud->getContentSize().width/2,pSprite->getContentSize().height*1.06f*k));
    int speed = arc4random()%10;
    CCMoveBy* fly = CCMoveBy::create(speed+18.0f, ccp(-SCREEN_WIDTH-cloud->getContentSize().width, 0.0f));
    CCSequence* seq = CCSequence::create(fly,CCCallFuncND::create(this, callfuncND_selector(MainMenu::removeCloud),cloud),NULL);
    cloud->runAction(seq);
}
void MainMenu::update_leaf(float dt)
{
    timer_leaf++;
    int rand_cloud = arc4random()%100+150;
    if (timer_leaf%rand_cloud == 0)
    {
        char* str = new char[10];
        sprintf(str, "leaf%d.png",arc4random()%4);
        CCSprite* leaf = CCSprite::create(str);
        main->addChild(leaf,10);
        leaf->setScale(SCALE_SCREEN_ORIGIN/2);
        float k = (arc4random()%10)/100.0f+0.85f;
        leaf->setPosition(ccp(SCREEN_WIDTH+leaf->getContentSize().width/2,SCREEN_HEIGHT*k));
        int speed = arc4random()%3+50;
        int direction = arc4random()%20+1;
        int rotate_direction = arc4random()%2==0 ? 1 : -1;
        CCMoveBy* fly = CCMoveBy::create(1.0f, ccp(-10*speed, -(speed*speed/direction)));
        CCRotateBy* rotate = CCRotateBy::create(1.0f, 360.0f*rotate_direction);
        CCRepeatForever* repeat1 = CCRepeatForever::create(fly);
        CCRepeatForever* repeat2 = CCRepeatForever::create(rotate);
        CCSequence* seq = CCSequence::create(CCDelayTime::create(5.0f),CCCallFuncND::create(this, callfuncND_selector(MainMenu::removeCloud),leaf),NULL);
        leaf->runAction(seq);
        leaf->runAction(repeat1);
        leaf->runAction(repeat2);
    }
}
void MainMenu::waiting_play(float dt)
{
    timer++;
    //    CCLog("timer = %d",timer);
    if (timer == 50) {
        character_hero->update_hero_state(state_jump);
        
    }
    if (timer == 60) {
        
        schedule(schedule_selector(MainMenu::start_game));
    }
    if (timer == 70) {
        main_hero->setRotation(0);
        hero_hight = main_hero->getPosition().y;
        character_hero->update_hero_state(state_down);
        body_hero = MudPiece::create_physics_hero(world, main_hero);
        body_hero->SetAwake(true);
    }
    
    
}
void MainMenu::enter_game(CCObject* sender)
{
    overlay->setVisible(false);
    body_apple->SetAwake(true);
    schedule( schedule_selector(MainMenu::tick));
    schedule(schedule_selector(MainMenu::remove_pastObject));
    schedule(schedule_selector(MainMenu::waiting_play));
    unschedule(schedule_selector(MainMenu::update_leaf));
    unschedule(schedule_selector(MainMenu::update));
    
}
void MainMenu::start_game(float dt)
{
    CCSize visiblesize = CCDirector::sharedDirector()->getVisibleSize();
    float screen_visibleX = CCDirector::sharedDirector()->getVisibleOrigin().x + visiblesize.width / 2;
    float screen_visibleY = CCDirector::sharedDirector()->getVisibleOrigin().y + visiblesize.height / 2;
    if (settings->collision_detected)
    {
        if (settings->dead_count%3 == 0 && settings->admob_show) {
            SocialBridge::sharedBridge()->showbanner();
        }
        
        settings->dead_count+=1;
        unscheduleAllSelectors();
        main_hero->stopAllActions();
        char text_score1[10];
        sprintf(text_score1, "%d",real_score);
        score_label_value->setString(text_score1);
        
        char text_score2[10];
        sprintf(text_score2, "%d",highscore);
        best_label_value->setString(text_score2);
        overlay_gameover->setVisible(true);
        main_hero->runAction(CCMoveTo::create(2.0f, ccp(main_hero->getPosition().x, main_hero->getPosition().y-visiblesize.height)));
        main_hero->runAction(CCRotateTo::create(2.0f, 1000.0f));
    }
    if (timer == 220)
        if (!CCUserDefault::sharedUserDefault()->getBoolForKey(IS_FIRST)) {
            
            tutorial_image->setVisible(true);
            tutorial_image->setPosition(ccp(screen_visibleX, screen_visibleY));
            CCDirector::sharedDirector()->pause();
            paused = true;
        }
    
    if (timer==250)
    {
        swipe_flag = true;
        schedule(schedule_selector(MainMenu::movetoCenter));
    }
    if (body_hero)
    {
        b2Vec2 hero_pos = body_hero->GetPosition();
        CCPoint current_hero_Pos = CCPoint(hero_pos.x * PTM_RATIO, hero_pos.y * PTM_RATIO);
        //        if (current_hero_Pos.y < SCREEN_HEIGHT/2)
        //            this->setPositionY(hero_hight-current_hero_Pos.y*1.2f);
        //
        //        else
        main->setPositionY(hero_hight-current_hero_Pos.y);
        body_hero->SetLinearVelocity(b2Vec2(0,hero_speed));
        if (score_pos_flag) {
            label_score->setVisible(true);
            if (IS_IPAD)
                label_score->setPosition(ccp(main_hero->getPosition().x+300.0f, main_hero->getPosition().y+590.0f));
            else
                label_score->setPosition(ccp(main_hero->getPosition().x+230.0f , main_hero->getPosition().y+590.0f));
        }
    }
    b2Body* mud1_body = (b2Body*)muds->objectAtIndex(muds->count()-2);
    b2Body* mud2_body = (b2Body*)muds->objectAtIndex(muds->count()-1);
    b2Vec2 mud1_vec = mud1_body->GetPosition();
    b2Vec2 mud2_vec = mud2_body->GetPosition();
    CCPoint mud1_pos = CCPoint(mud1_vec.x * PTM_RATIO, mud1_vec.y * PTM_RATIO);
    CCPoint mud2_pos = CCPoint(mud2_vec.x * PTM_RATIO, mud2_vec.y * PTM_RATIO);
    CCSprite* body_sprite_left = (CCSprite*)mud1_body->GetUserData();
    CCSprite* body_sprite_right = (CCSprite*)mud2_body->GetUserData();
    int type_mud_left = body_sprite_left->getTag();
    int type_mud_right = body_sprite_right->getTag();
    int type_left, type_right;
    float pos = main->getPositionY();
    
    if ( pos >= temp_pos)
    {
        if (timer<250) {
            type_left = type_mud1;
            type_right = type_mud2;
            
        }
        else
        {
            if (type_mud_left == type_mud1)         ///1 case
            {
                if (type_mud_right == type_mud2) {  ///1-2 case
                    int rand_next = arc4random()%10;
                    if (timer < 180) {
                        rand_next = 9;
                    }
                    if (rand_next == 1) {           ///5-2
                        type_left = type_mud5;
                        type_right = type_mud2;
                    }
                    else if (rand_next == 2)           ///1-3
                    {
                        type_left = type_mud1;
                        type_right = type_mud3;
                    }
                    else                               ///1-2
                    {
                        type_left = type_mud1;
                        type_right = type_mud2;
                    }
                }
                else if (type_mud_right == type_mud3) {  ///1-3 case
                    int rand_next = arc4random()%5;
                    if (rand_next == 0)                  /// 4-2
                    {
                        type_left = type_mud4;
                        type_right = type_mud2;
                    }
                    else                                 /// 4-3
                    {
                        type_left = type_mud4;
                        type_right = type_mud3;
                    }
                    mud1_pos = ccp(mud1_pos.x+89.0f,mud1_pos.y);
                    mud2_pos = ccp(mud2_pos.x+89.0f,mud2_pos.y);
                }
                else if (type_mud_right == type_mud6) {  ///1-6 case
                    type_left = type_mud1;
                    type_right = type_mud2;
                    
                }
                
            }
            else if (type_mud_left == type_mud4)     ///4 case
            {
                if (type_mud_right == type_mud3) {  ///4-3 case
                    int rand_next = arc4random()%5;
                    
                    if (rand_next == 4) {           ///4-2
                        type_left = type_mud4;
                        type_right = type_mud2;
                    }
                    else                            ///4-3
                    {
                        type_left = type_mud4;
                        type_right = type_mud3;
                    }
                    mud1_pos = ccp(mud1_pos.x+89.0f,mud1_pos.y);
                    mud2_pos = ccp(mud2_pos.x+89.0f,mud2_pos.y);
                }
                else if (type_mud_right == type_mud2) {  ///4-2 case
                    type_left = type_mud1;
                    type_right = type_mud2;
                    mud1_pos = ccp(mud1_pos.x,mud1_pos.y);
                }
            }
            else if (type_mud_left == type_mud5)     ///5 case
            {
                if (type_mud_right == type_mud6) {  ///5-6 case
                    int rand_next = arc4random()%4;
                    
                    if (rand_next == 0) {           ///1-6
                        type_left = type_mud1;
                        type_right = type_mud6;
                    }
                    else                            ///5-6
                    {
                        type_left = type_mud5;
                        type_right = type_mud6;
                    }
                    mud1_pos = ccp(mud1_pos.x-89.0f,mud1_pos.y);
                    mud2_pos = ccp(mud2_pos.x-89.0f,mud2_pos.y);
                }
                else if (type_mud_right == type_mud2) {  ///5-2 case
                    type_left = type_mud5;
                    type_right = type_mud6;
                    mud1_pos = ccp(mud1_pos.x-89.0f,mud1_pos.y);
                    mud2_pos = ccp(mud2_pos.x-89.0f,mud2_pos.y);
                }
                
            }
            
        }
        b2Body* mud_left = MudPiece::create_muds(main, world, mud1_pos,type_left);
        b2Body* mud_right = MudPiece::create_muds(main, world, mud2_pos, type_right);
        muds->addObject((CCObject*)mud_left);
        muds->addObject((CCObject*)mud_right);
        
        score++;
        if (score%15 ==0) {
            real_score++;
        }
        if (score_pos_flag) {
            char score_text[20];
            
            sprintf(score_text, "%d",real_score);
            label_score->setString(score_text);
            settings->current_score = real_score;
            if (!CCUserDefault::sharedUserDefault()->getBoolForKey(IS_FIRST))
            {
                highscore = real_score;
                settings->save_highscore(real_score);
                CCUserDefault::sharedUserDefault()->setBoolForKey(IS_FIRST, true);
            }
            else
            {
                highscore = settings->get_highscore();
                if (highscore<real_score)
                {
                    highscore = real_score;
                    settings->save_highscore(highscore);
                    highscore = settings->get_highscore();
                }
            }
            
            
        }
        temp_pos+=89.0f;
        //        CCLog("%f,,,,,%d,,,,,,%d",temp_pos,muds->count(),type_mud_left);
        
    }
    if (timer%500 == 0) {
        apple = CCSprite::create("apple.png");
        apple->setPosition(ccp((mud1_pos.x+mud2_pos.x)/2,mud1_pos.y));
        main->addChild(apple,100);
        schedule(schedule_selector(MainMenu::remove_apple));
    }
//    CCSprite* inner_temp = (CCSprite*)inners->objectAtIndex(inners->count()-1);
    if (pos >= inner_temp_pos-2.0f) {
        float po = inner_mud->getPosition().y;
        inner_mud = CCSprite::create("inner_mud.png");
        inner_mud->setPosition(ccp((mud1_pos.x+mud2_pos.x)/2,po-inner_mud->getContentSize().height));
        main->addChild(inner_mud,-1);
        inner_temp_pos+=583.0/2.0f;
        inners->addObject(inner_mud);
    }
    
    
}
void MainMenu::movetoCenter(float dt)
{
    float x_pos;
    bool temp_flag = true;
    if (temp_flag) {
        if (IS_IPAD)
            x_pos = SCREEN_WIDTH*0.23f;
        else
            x_pos = SCREEN_WIDTH*0.2f;
        temp_flag = false;
    }
    
    
    //    main_hero->setPosition(ccpAdd(main_hero->getPosition(), ccp(8.0f, 0.0f)));
    main->setPosition(ccpAdd(main->getPosition(), ccp(8.0f, 0.0f)));
    
    CCObject* object = NULL;
    if (main->getPosition().x>=x_pos)
    {
        unschedule(schedule_selector(MainMenu::movetoCenter));
        score_pos_flag = true;
    }
    
}
void MainMenu::remove_pastObject(float dt)
{
    //    if (muds->count()==30) {
    //        b2Body* removed_body1 = (b2Body*)muds->objectAtIndex(0);
    //        b2Body* removed_body2 = (b2Body*)muds->objectAtIndex(1);
    //        world->DestroyBody(removed_body1);
    //        world->DestroyBody(removed_body2);
    //        removed_body1->SetUserData(NULL);
    //        removed_body2->SetUserData(NULL);
    //        muds->removeObjectAtIndex(0);
    //        muds->removeObjectAtIndex(1);
    //
    //    }
}
void MainMenu::collision_detect()
{
    CCObject* object = NULL;
    CCARRAY_FOREACH(muds, object){
        //        CCSprite* obj = (CCSprite*)object;
        //        obj->setPosition(ccpAdd(obj->getPosition(), ccp(0.0f, 8.0f*SCALE_SCREEN_ORIGIN)));
    }
}

void MainMenu::create_mud(float dt)
{
    //    MudPiece* mud = new MudPiece(ccp(pSprite->getPosition().x,0.0f),0);
    //    this->addChild(mud->getMud());
}
void MainMenu::removeCloud(CCObject* sender)
{
    CCSprite* clo = (CCSprite*)sender;
    clo->removeFromParentAndCleanup(true);
}
void MainMenu::remove_label(CCObject* sender)
{
    CCLabelTTF* label = (CCLabelTTF*)sender;
    label->removeFromParentAndCleanup(true);
}
void MainMenu::remove_apple(float dt)
{
    CCRect apple_rect = apple->boundingBox();
    CCRect hero_rect = main_hero->boundingBox();
    if (apple_rect.intersectsRect(hero_rect))
    {
        unschedule(schedule_selector(MainMenu::remove_apple));
        hero_speed--;
        CCLabelTTF* speed_up = CCLabelTTF::create("Speed Up+", "", 48);
        speed_up->setPosition(apple->getPosition());
        main->addChild(speed_up,100);
        speed_up->runAction(CCSequence::create(CCFadeOut::create(1.0f),CCCallFuncND::create(this, callfuncND_selector(MainMenu::remove_label),speed_up),NULL));
        apple->removeFromParentAndCleanup(true);
    }
}

void MainMenu::draw(){
    CCLayer::draw();
    
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
    kmGLPushMatrix();
    world->DrawDebugData();
    kmGLPopMatrix();
}
void MainMenu::tick(float dt)
{
    int velocityIterations = 8;
    int positionIterations = 1;
    
    // Instruct the world to perform a single step of simulation. It is
    // generally best to keep the time step and iterations fixed.
    world->Step(dt, velocityIterations, positionIterations);
    
    //Iterate over the bodies in the physics world
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            //Synchronize the AtlasSprites position and rotation with the corresponding body
            CCSprite* myActor = (CCSprite*)b->GetUserData();
            if (myActor != main_hero) {
                myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
                
            }
            myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
            
        }
    }
    body_apple->SetAwake(true);
    
    //    b2Body* temp1 = (b2Body*)muds->objectAtIndex(muds->count()-2);
    //    b2Body* temp2 = (b2Body*)muds->objectAtIndex(muds->count()-1);
    
    if (timer >= 70) {
        //this->setPositionY(SCREEN_HEIGHT-currentApplePos.y);
    }
    //    body_apple->SetLinearVelocity(b2Vec2(0,-3));
    //
    //    CCPoint currentApplePos = CCPoint(apple_pos.x * PTM_RATIO, apple_pos.y * PTM_RATIO);
    //    this->setPositionY(SCREEN_HEIGHT*0.58f-currentApplePos.y);
    
    //    this->setPosition(ccpAdd(this->getPosition(), ccp(0.0f, 3)));
    
}

void MainMenu::init_swipeGesture()
{
    CCSwipeGestureRecognizer* L2R = CCSwipeGestureRecognizer::create();
    L2R->setTarget(this, callfuncO_selector(MainMenu::swipe_L2R));
    L2R->setDirection(kSwipeGestureRecognizerDirectionRight);
    //    L2R->setCancelsTouchesInView(true);
    L2R->setTag(s_L2R);
    this->addChild(L2R);
    
    CCSwipeGestureRecognizer* R2L = CCSwipeGestureRecognizer::create();
    R2L->setTarget(this, callfuncO_selector(MainMenu::swipe_R2L));
    R2L->setDirection(kSwipeGestureRecognizerDirectionLeft);
    //    R2L->setCancelsTouchesInView(true);
    R2L->setTag(s_R2L);
    this->addChild(R2L);
    
    
    CCSwipeGestureRecognizer* T2B = CCSwipeGestureRecognizer::create();
    T2B->setTarget(this, callfuncO_selector (MainMenu::swipe_T2B));
    T2B->setDirection(kSwipeGestureRecognizerDirectionDown);
    //    T2B->setCancelsTouchesInView(true);
    T2B->setTag(s_T2B);
    this->addChild(T2B);
    
    CCSwipeGestureRecognizer* B2T = CCSwipeGestureRecognizer::create();
    B2T->setTarget(this, callfuncO_selector(MainMenu::swipe_B2T));
    B2T->setDirection(kSwipeGestureRecognizerDirectionUp);
    //    B2T->setCancelsTouchesInView(true);
    B2T->setTag(s_B2T);
    this->addChild(B2T);
    
}
void MainMenu::swipe_L2R(CCObject* sender)
{
    if (!settings->collision_detected && body_hero && swipe_flag)
    {
        body_apple->SetTransform(body_apple->GetPosition(),CC_DEGREES_TO_RADIANS(45));
        body_hero->SetTransform(body_hero->GetPosition(), CC_DEGREES_TO_RADIANS(45));
        character_hero->update_hero_state(state_rightdown);
        unschedule(schedule_selector(MainMenu::moving_left));
        schedule(schedule_selector(MainMenu::moving_right));
    }
}
void MainMenu::swipe_R2L(CCObject *sender)
{
    if (!settings->collision_detected && body_hero && swipe_flag)
    {
        body_apple->SetTransform(body_apple->GetPosition(), CC_DEGREES_TO_RADIANS(-45));
        body_hero->SetTransform(body_hero->GetPosition(), CC_DEGREES_TO_RADIANS(-45));
        character_hero->update_hero_state(state_leftdown);
        unschedule(schedule_selector(MainMenu::moving_right));
        schedule(schedule_selector(MainMenu::moving_left));
    }
}
void MainMenu::swipe_T2B(CCObject *sender)
{
    if (!settings->collision_detected && body_hero  && swipe_flag)
    {
        body_apple->SetTransform(body_apple->GetPosition(), 0);
        character_hero->update_hero_state(state_down);
        unschedule(schedule_selector(MainMenu::moving_left));
        unschedule(schedule_selector(MainMenu::moving_right));
        b2Vec2 speed = body_hero->GetLinearVelocity();
        body_hero->SetLinearVelocity(b2Vec2(0,speed.y));
        body_hero->SetAngularVelocity(0);
        body_hero->SetTransform(body_hero->GetPosition(), CC_DEGREES_TO_RADIANS(0));
        body_hero->SetFixedRotation(false);
    }
    
}
void MainMenu::swipe_B2T(CCObject *sender)
{
    
    CCLog("this is swipe action.....");
}
void MainMenu::moving_left(float dt)
{
    b2Vec2 speed = body_hero->GetLinearVelocity();
    body_hero->SetLinearVelocity(b2Vec2(speed.y,speed.y));
    b2Vec2 hero_pos = body_hero->GetPosition();
    main->setPositionX(SCREEN_WIDTH/2-hero_pos.x*PTM_RATIO);
    
    body_hero->SetTransform(body_hero->GetPosition(), CC_DEGREES_TO_RADIANS(-45));
    
}
void MainMenu::moving_right(float dt)
{
    b2Vec2 speed = body_hero->GetLinearVelocity();
    body_hero->SetLinearVelocity(b2Vec2(speed.y*(-1),speed.y));
    b2Vec2 hero_pos = body_hero->GetPosition();
    main->setPositionX(SCREEN_WIDTH/2-hero_pos.x*PTM_RATIO);
    body_hero->SetTransform(body_hero->GetPosition(), CC_DEGREES_TO_RADIANS(45));
    
}
void MainMenu::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event) {
    
}
void MainMenu::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event) {
    if (!CCUserDefault::sharedUserDefault()->getBoolForKey(IS_FIRST) && paused)
    {
        CCDirector::sharedDirector()->resume();
        paused = false;
        tutorial_image->setVisible(false);
    }
    if (is_scoreboard) {
        overlay->setVisible(true);
        overlay_score->setVisible(false);
        is_scoreboard = false;
    }
}
void MainMenu::replay_game(CCObject *sender)
{
    muds->removeAllObjects();
    main->removeAllChildrenWithCleanup(true);
    main->removeFromParentAndCleanup(true);
    
    settings->collision_detected = false;
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, MainMenu::scene()));
    
}
void MainMenu::share_game(CCObject* sender)
{
    SocialBridge::share_score();
}
void MainMenu::highscore_menu(CCObject* sender)
{
    is_scoreboard = true;
    char text_score1[10];
    sprintf(text_score1, "%d",settings->current_score);
    score_label_value_main->setString(text_score1);
    
    char text_score2[10];
    sprintf(text_score2, "%d",settings->get_highscore());
    best_label_value_main->setString(text_score2);
    
    overlay_score->setVisible(true);
    overlay->setVisible(false);
    CCLog("This is high score : %d", settings->get_highscore());
}






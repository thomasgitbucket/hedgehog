//
//  SocailBridge.h
//  SwipyHedgehog
//
//  Created by Leander on 27/01/15.
//
//

#ifndef __SwipyHedgehog__SocailBridge__
#define __SwipyHedgehog__SocailBridge__

#include "cocos2d.h"
USING_NS_CC;
class SocialBridge
{
private:
    SocialBridge();
    ~SocialBridge();
public:
    static SocialBridge* instance;
    static SocialBridge* sharedBridge();
    void showbanner();
    void hidebanner();
    void disable_ads();
    static void share_score();
};
#endif /* defined(__SwipyHedgehog__SocailBridge__) */

//
//  SplashScene.h
//  SwipyHedgehog
//
//  Created by Leander on 12/11/14.
//
//

#ifndef __SwipyHedgehog__SplashScene__
#define __SwipyHedgehog__SplashScene__

#include "cocos2d.h"
using namespace cocos2d;
class SplashScene : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void enter_menu();
    
    // implement the "static node()" method manually
    CREATE_FUNC(SplashScene);
};

#endif /* defined(__SwipyHedgehog__SplashScene__) */

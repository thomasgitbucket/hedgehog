//
//  CollisionListner.cpp
//  SwipyHedgehog
//
//  Created by lion on 1/25/15.
//
//

#include "CollisionListner.h"
#include "cocos2d.h"
using namespace cocos2d;
CollisionListner::CollisionListner()
{
    settings = GameSetting::sharedGameSetting();
}
CollisionListner::~CollisionListner()
{
    
}
void CollisionListner::BeginContact(b2Contact *contact)
{
    settings->collision_detected = true;
    CCLog("collision begincontact");
}
void CollisionListner::EndContact(b2Contact *contact)
{
    CCLog("collision Endcontact");
}
void CollisionListner::PreSolve(b2Contact *contact, const b2Manifold* oldManifold)
{

}
void CollisionListner::PostSolve(b2Contact *contact, const b2ContactImpulse *impulse)
{
    
}
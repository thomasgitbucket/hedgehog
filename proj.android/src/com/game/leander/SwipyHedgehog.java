/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.game.leander;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest.ErrorCode;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class SwipyHedgehog extends Cocos2dxActivity {
	public static native void disableAds();
	public static InterstitialAd interstitial = null;
	public static SwipyHedgehog sh=null;
	private  AdView adView;
	private static final String MY_AD_UNIT_ID = "ca-app-pub-2144507262071986/6849181956";
	private static RelativeLayout relativeLayout;
	private static Cocos2dxGLSurfaceView surfaceview = null;
	private AdListener adListener = new AdListener() {
		@Override
		public void onAdFailedToLoad( int errorCode) {
			String message = "Load Ads Failed: (" + errorCode + ")";
			Log.d("error", message);
		}
		@Override
		public void onAdLoaded() {
			Log.d("error", "loaded");
		}
		@Override
		public void onAdOpened() {
			Log.d("error", "opened");
		}
		@Override
		public void onAdLeftApplication(){
			disableAds();
		}
		

	};
	
    protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);	
		sh = this;
		
		interstitial = new InterstitialAd(this);
		
		interstitial.setAdUnitId(MY_AD_UNIT_ID);
		interstitial.setAdListener(adListener);
//		adView = new AdView(this);
//		adView.setAdSize(AdSize.SMART_BANNER);
//		adView.setAdUnitId(MY_AD_UNIT_ID);
		
		
//		adView.setBackgroundColor(Color.GRAY);
//		relativeLayout = new RelativeLayout(this);
//		RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(AdView.LayoutParams.FILL_PARENT, AdView.LayoutParams.WRAP_CONTENT);
//		relativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//		relativeLayout.addView(adView, relativeLayoutParams);
//		framelayout.addView(relativeLayout);
//		adView.setVisibility(View.INVISIBLE);
    }

    public Cocos2dxGLSurfaceView onCreateView() {
    	Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
    	// SwipyHedgehog should create stencil buffer
    	glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
    	
    	surfaceview = glSurfaceView;
    	
    	return glSurfaceView;
    }

    static {
        System.loadLibrary("cocos2dcpp");
    }     
    
    public static void showAds()
    {
    	

    	sh.runOnUiThread(new Runnable()
    	{
    		
			@Override
			public void run() {
		    	AdRequest adRequest = new AdRequest.Builder().build();
				interstitial.loadAd(adRequest);
				
				if(interstitial.isLoaded())
		    	{
		    		interstitial.show();
		    	}
			}
		});
    }
    public static void hideAds()
    {   
    	

//    	sh.runOnUiThread(new Runnable() 
//    	{
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				if (sh.adView.isEnabled())
//				{
//					sh.adView.setEnabled(false);
//				}
//				if (sh.adView.getVisibility() !=4) 
//				{
//					sh.adView.setVisibility(View.INVISIBLE);
//				}
//			}
//		});
    }
    public static void shareScore(int score)
    {   
    	
    	Intent shareIntent=new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,"My recent score of Hedgehog Game is "+String.format("%d", score));
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "The title");
        sh.startActivity(Intent.createChooser(shareIntent, "Share..."));
        
//    	Uri imageUri;
//    	 Intent intent;
//
//    	        imageUri = takeScreenshot();
//    	        if(imageUri != null)
//    	        {
//    	        intent = new Intent();
//    	        intent.setAction(Intent.ACTION_SEND);
//    	        intent.putExtra(Intent.EXTRA_TEXT, "Hello");
//    	        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
//    	        intent.setType("image/*");
//    	        sh.startActivity(intent);
//    	        }
//        
        
        
    }
    
    
    
    public static Uri takeScreenshot( )
    {
    	Bitmap bitmap = loadBitmapFromView(sh, surfaceview); //get Bitmap from the view
    	
        String mPath = Environment.getExternalStorageDirectory() + File.separator + "screen_" + System.currentTimeMillis() + ".jpeg";
        File imageFile = new File(mPath);
        OutputStream fout = null;
        try {
            fout = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
            fout.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
				fout.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

    	Uri uri = Uri.fromFile(new File(mPath));
    	return uri;
    }
    public static Bitmap loadBitmapFromView(Context context, View v) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics(); 
        v.measure(MeasureSpec.makeMeasureSpec(dm.widthPixels, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(dm.heightPixels, MeasureSpec.EXACTLY));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap returnedBitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(returnedBitmap);
        v.draw(c);

        return returnedBitmap;
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();

    }
    @Override
    protected void onResume()
    {
    	super.onResume();
    	if (adView != null) 
    	{
    		adView.resume();			
		}
    }
    @Override
    protected void onStop() 
    {
		super.onStop();

	}
    @Override
    protected void onPause()
    {
    	if (adView != null) 
    	{
    		adView.pause();			
		}
    	super.onPause();
    }
    @Override
    protected void onDestroy()
    {
    	adView.destroy();
    	super.onDestroy();
    }
    
}
